import * as React from 'react'

const InputString= ({label, defaultValue, onChange}) =>{
  return <label>
    {label} : <input type="text" defaultValue={defaultValue} className="form-control" onChange={onChange}/>
  </label>
}

export default InputString