import React, {Component} from 'react';
import {BrowserRouter, Route, Link} from 'react-router-dom';
import Home from './Home';
import NewPlayerForm from './NewPlayerForm'
import EditPlayerForm from './EditPlayerForm'
import FindPlayerForm from './FindPlayerForm'

class Navigation extends Component{
  render(){
    return(
        <BrowserRouter>
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <Link to="/" className="nav-link">Home</Link>
                </li>
                <li class="nav-item">
                  <Link to="/newplayer" className="nav-link">New Player</Link>
                </li>
                <li class="nav-item">
                  <Link to="/editplayer" className="nav-link">Edit Player</Link>
                </li>
                <li class="nav-item">
                  <Link to="/findplayer" className="nav-link">Find Player</Link>
                </li>
              </ul>
            </div>
          </nav>

          <div>
            <Route exact path="/" component={Home}/>
            <Route path="/newplayer" component={NewPlayerForm}/>
            <Route path="/editplayer" component={EditPlayerForm}/>
            <Route path="/findplayer" component={FindPlayerForm}/>
          </div>
      </BrowserRouter>
    )
  }
}

export default Navigation;