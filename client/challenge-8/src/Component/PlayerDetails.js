import * as React from 'react'

const PlayerDetails = ({username, email, experience, level}) =>{

  return <div>
    <h2 className="py-5 text-center"> Player Details: </h2>
    <table className="table">
      <thead>
        <tr>
          <th scope="col">Username</th>
          <th scope="col">Email</th>
          <th scope="col">Experience</th>
          <th scope="col">Level</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{username}</td>
          <td>{email}</td>
          <td>{experience}</td>
          <td>{level}</td>
        </tr>
      </tbody>
    </table>
  </div>
}

export default PlayerDetails