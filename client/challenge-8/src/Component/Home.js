import {Component} from 'react';
import Navigation from './Navigation';

class Home extends Component{
  render(){
    return(
      <div>
        <h1 className="left-0 text-center bg-faded p-5 rounded">Home</h1>
      </div>
    )
  }
}

export default Home