import {Component} from 'react';
import Form from './Form';

class FindPlayerForm extends Component{
  render(){
    return(
      <div>
        <h1 className="left-0 text-center bg-faded p-3 rounded">FIND PLAYER FORM</h1>
        <Form />
      </div>
    )
  }
}

export default FindPlayerForm