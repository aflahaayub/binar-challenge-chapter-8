import {Component} from 'react';
import Form from './Form';

class NewPlayerForm extends Component{
  render(){
    return(
      <div>
        <h1 className="left-0 text-center bg-faded p-3 rounded">NEW PLAYER FORM</h1>
        <Form />
      </div>
    )
  }
}

export default NewPlayerForm