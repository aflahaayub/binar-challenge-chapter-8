import * as React from 'react';
import InputString from './InputString';
import PlayerDetails from './PlayerDetails';

function Form(){
  const [form, setForm] = React.useState({
    username: '',
    email: '',
    experience: '',
    lvl: 0
  })

  let players;
  const HandleSubmit = (event) =>{
    event.preventDefault();
    alert('Succeeded!');
  }

  return <div className="container"> 
    <form onSubmit={HandleSubmit}>
      <div className="form-group px-3 py-2">
        <InputString label="Username" defaultValue={form.username} onChange={(e)=> setForm({...form, username: e.target.value})}/>
      </div>
      <div className="form-group px-3 py-1">
        <InputString label="Email" defaultValue={form.email} onChange={(e)=> setForm({...form, email: e.target.value})}/>
      </div>
      <div className="form-group px-3 py-1">
        <InputString label="Experience" defaultValue={form.experience} onChange={(e)=> setForm({...form, experience: e.target.value})}/>
      </div>
      <div className="form-group px-3 py-1">
        <label>Level: </label>
        <input type="number" className="form-control" defaultValue={form.lvl} onChange={(e) => setForm({...form, lvl:e.target.value})} />
      </div>
      <button type="submit" className="mx-3 my-1 btn btn-primary">Submit</button>
      <PlayerDetails username={form.username} email= {form.email}  experience={form.experience} level={form.lvl} />
    </form>
  </div>
}


export default Form;