// import './App.css';

import Navigation from "./Component/Navigation";
import NewPlayerForm from "./Component/NewPlayerForm";

function App() {
  return (
    <div>
      <Navigation />
    </div>
  );
}

export default App;
